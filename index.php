<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
    <header class="header-home -aux-home">
        <nav class="navbar navbar-expand-lg navbar-light bg-green-menu">
            <div class="container">
                <div class="navbar-collapse">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php"><img src="assets/images/clara.png" width="100px"/></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <?php 
        $bgSlide = "santa-video.jpg";
        $titleSlide = "<a href='#.' data-toggle='modal' data-target='#youtubeVideo'><img src='assets/images/play.png' width='80px' class='img-fluid'/></a>";
        include("includes/slide.php"); ?>
    </header>
    
    <!-- Modal -->
    <div class="modal fade" id="youtubeVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/i2JmVSqxHrw" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <a href="#" class="btn-outline-green" data-dismiss="modal">Fechar vídeo</a>
                </div>
            </div>
        </div>
    </div>
    
    <section class="seals" id="seals">
        <div class="container">
            <div class="row">
                <div class="col-4 col-xl-2 offset-xl-3 align-self-center">
                    <a href="#.">
                        <img src="assets/images/selo-01.png" class="img-fluid"/>
                    </a>
                </div>
                <div class="col-4 col-xl-2 align-self-center">
                    <a href="#.">
                        <img src="assets/images/selo-02.png" class="img-fluid"/>
                    </a>
                </div>
                <div class="col-4 col-xl-2 align-self-center">
                    <a href="#.">
                        <img src="assets/images/selo-03.png" class="img-fluid"/>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="know">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Grupo Clara Resorts</h1>
                    <p class="description">Conheça nossos resorts</p>
                    <div class="card-deck">
                        <div class="row">
                            <div class="col-xl-6 mb-2">
                                <a href="http://www.deepocean.com.br/clara-resorts/resort/" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/grupo-eco.jpg" class="img-fluid"/>
                                </a>
                            </div>

                            <div class="col-xl-6 mb-2">
                                <a href="http://www.deepocean.com.br/clara-resorts/ibiuna/" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/grupo-ibiuna.jpg" class="img-fluid"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <header class="header-home">
    <?php 
        $bgSlide = "slide-01.jpg";
        $titleSlide = "Histórias <br/> <span>em familia</span>";
        include("includes/slide.php"); ?>
   </header>
    

    <section class="events-01">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h2 class="title-master">Nosso Propósito</h2>
                    <div class="swiper-container swiper-events-01">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="assets/images/fundacao.jpg" class="img-fluid">
                                <h1 class="title">Fundacao</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/fundacao.jpg" class="img-fluid">
                                <h1 class="title">Fundacao</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/fundacao.jpg" class="img-fluid">
                                <h1 class="title">Fundacao</h1>
                            </div>
                        </div>
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>

                    <p class="description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. 
                    </p>
                </div>
            </div>
        </div>
    </section>
   
    <?php include("includes/gallery.php")?>
    <section class="events">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 text-center">
                    <h2 class="title-master">Eventos</h2>
                    <div class="swiper-container swiper-events">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="assets/images/convencoes.jpg" class="img-fluid">
                                <h1 class="title">Convenções</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/convencoes.jpg" class="img-fluid">
                                <h1 class="title">Convenções</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/convencoes.jpg" class="img-fluid">
                                <h1 class="title">Convenções</h1>
                            </div>
                        </div>
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>

                    <a href="eventos.php" class="btn-outline-green mt-4 mb-4 d-block d-xl-none">Ver todos os eventos</a>
                </div>

                <div class="col-xl-6 text-center">
                    <h2 class="title-master">Sustentabilidade e Social</h2>
                    <div class="swiper-sust">
                        <img src="assets/images/criancas.jpg" class="img-fluid">
                        <p class="description">No Santa Clara Eco Resort a preocupação com o meio ambiente vai muito além de ações pontuais: é parte do DNA do resort</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

    <?php include("includes/testmonials.php")?>
    
    <?php include("includes/footer.php")?>
    <?php include("includes/scripts.php")?>
</body>
</html>