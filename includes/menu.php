<nav class="navbar navbar-expand-lg navbar-light bg-green-menu">
            <div class="container">
                <div class="navbar-collapse">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php"><img src="assets/images/logo.png" width="150px"/></a>
                        </li>
                    </ul>
    
                    <div class="my-2 my-lg-0 d-none d-lg-block">
                        <a class="nav-link white-btn" href="reservas.php" data-toggle="modal" data-target="#ExemploModalCentralizado">Faça sua reserva</a>
                    </div>
                </div>
            </div>
        </nav>
    
        <nav class="navbar navbar-expand-lg navbar-dark bg-green-light d-none d-xl-block p-0">
            <div class="container">
                <div class="navbar-collapse" id="conteudoNavbarSuportado">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="resort.php">O resort</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="acomodacoes.php">Acomodações</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="atividades-lazer.php">Atividades e lazer</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="atividades-lazer.php">de crianças a adultos</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="programacao.php">programação</a>
                        </li>
                    </ul>
    
                    <div class="my-2 my-lg-0">
                        <div class="nav-item">
                            <div class="dropdown nav-drop">
                                <a href="#" class="drop-menu" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-chevron-down"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="promocao.php">Promoções e pacotes</a>
                                    <a class="dropdown-item" href="eventos.php">Eventos</a>
                                    <a class="dropdown-item" href="gastronomia.php">Gastronomia</a>
                                    <a class="dropdown-item" href="contato.php">Fale conosco</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-mobile navbar-expand-lg navbar-dark bg-green-light d-block d-xl-none">
            <div class="container">
                <div class="navbar-collapse">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a class="nav-link menu-open-mobile" onclick="openNav()"> <?php echo $label?> <i class="fas fa-bars"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="myNav" class="overlay">
            <div class="text-center">
                <p class="label-menu">menu</p>
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            </div>
            <div class="overlay-content">
                <a href="resort.php">O Resort</a>
                <a href="acomodacoes.php">Acomodações</a>
                <a href="atividades-lazer.php">Atividades e lazer</a>
                <a href="atividades-lazer.php">De crianças a adultos</a>
                <a href="programacao.php">Programação</a>
                <a href="promocao.php">Promoções e pacotes</a>
                <a href="eventos.php">Eventos</a>
                <a href="gastronomia.php">Gastronomia</a>
                <a href="contato.php">Fale conosco</a>
            </div>
        </div>
    
    
        <div class="bar-reserve d-block d-xl-none">
            <a href="reserva.php" data-toggle="modal" data-target="#ExemploModalCentralizado">Faça sua reserva</a>
        </div>



        <!-- Modal -->
        <div class="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TituloModalCentralizado">Título do modal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="lala"></div>
                </div>
                </div>
            </div>
        </div>