<section class="activities">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Atividades</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-6 text-center">
                    <h2 class="title-master">Atividades e lazer</h2>
                    <div class="swiper-container swiper-activities">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="assets/images/hipismo.jpg" class="img-fluid">
                                <h1 class="title">Hipismo</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/hipismo.jpg" class="img-fluid">
                                <h1 class="title">Hipismo</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/hipismo.jpg" class="img-fluid">
                                <h1 class="title">Hipismo</h1>
                            </div>
                        </div>
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>

                    <a href="atividades.php" class="btn-outline-green mt-4 mb-4 d-block d-xl-none">Ver todas as atividades</a>
                </div>

                <div class="col-xl-6 text-center">
                    <h2 class="title-master">De crianças a adultos</h2>
                    <div class="swiper-container swiper-child">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="assets/images/criancas.jpg" class="img-fluid">
                                <h1 class="title">Clubinho</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/criancas.jpg" class="img-fluid">
                                <h1 class="title">Clubinho</h1>
                            </div>

                            <div class="swiper-slide">
                                <img src="assets/images/criancas.jpg" class="img-fluid">    
                                <h1 class="title">Clubinho</h1>
                            </div>
                        </div>
                        <div class="swiper-counter"></div>
                        <div class="arrows">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>

                    <a href="atividades-lazer.php" class="btn-outline-green mt-4 mb-4 d-block d-xl-none">Ver todas as atividades</a>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-12 text-center">
                    <a href="atividades-lazer.php" class="btn-outline-green mt-4 d-none d-xl-inline-block">Ver todas as atividades</a>
                </div>
            </div>
        </div>
    </section>