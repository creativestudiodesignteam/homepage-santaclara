<!-- Meta tags Obrigatórias -->
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/jsRapCalendar.css" />

    <link rel="stylesheet" href="assets/css/simplelightbox.min.css" />
    <link rel="stylesheet" href="node_modules/swiper/css/swiper.min.css" />
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />

    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

    <title>Santa Clara - Resorts</title>

    <script src="node_modules/jquery/dist/jquery.min.js"></script>  
    <script src="assets/js/jsRapCalendar.js"></script>


    <script>
      $(document).ready(function(){
        $('#lala').jsRapCalendar({
            week:6,
                onClick:function(y,m,d){
                    alert(y + '-' + m + '-' + d);
                }
            });
        });
    </script>