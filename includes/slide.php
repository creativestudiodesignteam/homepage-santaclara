<div class="swiper-container swiper-home position-relative">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image:url('assets/images/<?php echo $bgSlide?>');">
            <h1 class="title">
                <?php echo $titleSlide?>
            </h1>
        </div>
        
        <div class="swiper-slide" style="background-image:url('assets/images/<?php echo $bgSlide?>');">
            <h1 class="title">
                <?php echo $titleSlide?>
            </h1>
        </div>

        <div class="swiper-slide" style="background-image:url('assets/images/<?php echo $bgSlide?>');">
            <h1 class="title">
                <?php echo $titleSlide?>
            </h1>
        </div>

        <div class="swiper-slide" style="background-image:url('assets/images/<?php echo $bgSlide?>');">
            <h1 class="title">
                <?php echo $titleSlide?>
            </h1>
        </div>
    </div>
    <a href="#seals" class="down scroll"><img src="assets/images/arrow-down.png"></a>
    <!-- Add Arrows -->
    <div class="d-none d-xl-block">
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
    <!-- Add Pagination -->
</div>
<div class="swiper-pagination"></div>