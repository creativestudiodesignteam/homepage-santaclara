<section class="accommodations">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h1 class="title">Acomodações</h1>
                    <div class="card-deck">
                        <div class="row">
                            <div class="col-xl-4">
                                <a href="interna-acomodacoes.php" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/acomodacao-01.jpg" class="img-fluid"/>
                                    
                                    <div class="row no-gutters icons">
                                        <div class="col-12">
                                            <i class="fas fa-shower"></i>
                                            <i class="fas fa-blind"></i>
                                            <i class="fas fa-fire"></i>
                                        </div>
                                    </div>
        
                                    <div class="card-body">
                                        <h1 class="card-title m-0">Apto vila do lago</h1>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-4">
                                <a href="interna-acomodacoes.php" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/acomodacao-02.jpg" class="img-fluid"/>
                                    
                                    <div class="row no-gutters icons">
                                        <div class="col-12">
                                            <i class="fas fa-shower"></i>
                                            <i class="fas fa-blind"></i>
                                            <i class="fas fa-fire"></i>
                                        </div>
                                    </div>
        
                                    <div class="card-body">
                                        <h1 class="card-title m-0">Suite Santa Clara</h1>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-4">
                                <a href="interna-acomodacoes.php" class="card position-relative">
                                    <img class="card-img-top" src="assets/images/acomodacao-01.jpg" class="img-fluid"/>
                                    
                                    <div class="row no-gutters icons">
                                        <div class="col-12">
                                            <i class="fas fa-shower"></i>
                                            <i class="fas fa-blind"></i>
                                            <i class="fas fa-fire"></i>
                                        </div>
                                    </div>
        
                                    <div class="card-body">
                                        <h1 class="card-title m-0">Chalé Luxo Plus</h1>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>

                    <a href="acomodacoes.php" class="btn-outline-green mt-4">Ver todas as acomodações</a>
                </div>
            </div>
        </div>
    </section>